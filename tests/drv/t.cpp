#include <gtest/gtest.h>
#include "helper.h"

TEST(drvHelperTest, find)
{
  EXPECT_EQ(2, h_find("8sq", 'q', 1));
  EXPECT_EQ(4, h_find("8sqaqq", 'q', 2));
  EXPECT_EQ(-1, h_find("8sqaq", 'p', 1));
  EXPECT_EQ(-1, h_find("8sqaq", 'q', 3));

}

TEST(drvHelperTest, getdivide)
{
  EXPECT_EQ(8, h_getdivide("SX T,1,1.9,8,180"));
  EXPECT_EQ(-1, h_getdivide("SX T,1,1.9,8"));
  EXPECT_EQ(-1, h_getdivide("SX T,1,1.9,,"));
  EXPECT_EQ(8, h_getdivide("SX T,1,1.9,8,180,3"));
  
}

TEST(drvHelperTest, getxy)
{
	char x[255], y[255];
	memset(x,'\0',sizeof(x)/sizeof(char));
	memset(y,'\0',sizeof(y)/sizeof(char));
	bool res = getxy("dsds\rdwdg2", x, y);
	ASSERT_TRUE(strcmp("dsds", x)==0);
  ASSERT_TRUE(strcmp("dwdg2", y)==0);
	ASSERT_TRUE(res);
}