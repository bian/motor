#include <gtest/gtest.h>
#include "motorhelper.h"

TEST(motorHelperTest, h_fill_mX1)
{
	std::vector<double> x;
	std::vector<double> y;
	h_fill_mX1(x, y, 30);
	EXPECT_EQ(30, x.size());
	EXPECT_EQ(30, y.size());
	EXPECT_EQ(21, x[20]);
	EXPECT_EQ(0, y[20]);
}
TEST(motorHelperTest, h_fill_mXn)
{
    std::vector<std::vector<double>> x;
    std::vector<std::vector<double>> y;
	h_fill_mXn(x, y, 3, 34);
	EXPECT_EQ(3, x.size());
	EXPECT_EQ(3, y.size());
	EXPECT_EQ(34, x[0].size());
	EXPECT_EQ(34, y[0].size());
	EXPECT_EQ(14, x[1].at(13));
	EXPECT_EQ(0, y[1].at(13));
}

TEST(motorHelperTest, h_seekPeak)
{
	std::vector<double> y;
	y.push_back(8);
    y.push_back(10);
    y.push_back(10);
    y.push_back(7);

	int id = h_seekPeak(y);
	EXPECT_EQ(2, id);
	y.push_back(17);
	id = h_seekPeak(y);
	EXPECT_EQ(4, id);
	y.push_back(17);
	id = h_seekPeak(y);
	EXPECT_EQ(5, id);
}