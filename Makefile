all:libsmake drvmake motormake 
	@echo ---- Done. ----
t: testmake
p: prepare
r: clean all
motormake:
	make -C motor
libsmake: 
	make -C libs
drvmake:
	make -C drv
testmake:
	make -C test 
prepare:
	@-copy %wxwin%\\lib\gcc_dll\\*.dll bin /y
clean:
	@-del /S /Q bin libs\lib motor\gcc_ms*