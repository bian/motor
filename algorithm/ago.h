#pragma once
#include "Psapi.h"
#include "windows.h"

#define print(num) do{char a[255]; sprintf(a, "print %d",num);MessageBoxA(0,a,"test", 0);}while(0)

inline DWORD FindProcess(const char *strProcessName)
{
    DWORD aProcesses[1024], cbNeeded, cbMNeeded;
    HMODULE hMods[1024];
    HANDLE hProcess;
    char szProcessName[MAX_PATH];

    if ( !EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) ) return 0;
    for(int i=0; i< (int) (cbNeeded / sizeof(DWORD)); i++)
    {
        hProcess = OpenProcess( PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, aProcesses[i]);
        EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbMNeeded);
        GetModuleFileNameExA( hProcess, hMods[0], szProcessName,sizeof(szProcessName));
        
        //print(aProcesses[i]);
        if(strstr(szProcessName, strProcessName) && (GetCurrentProcessId()!=aProcesses[i]))
        {
            return(aProcesses[i]);
        }
    }


    return 0;
}

inline bool KillProcess(DWORD& p)
{
    // When the all operation fail this function terminate the "winlogon" Process for force exit the system.
    HANDLE hYourTargetProcess = OpenProcess(PROCESS_TERMINATE,FALSE, p);

    if(hYourTargetProcess == NULL){
        return false;
	}
    if(TerminateProcess(hYourTargetProcess, 0)){
        return true;
      }
    return false;
}

inline bool check_exit(){
  const char *name="Motor.exe";
  DWORD pr = FindProcess(name);
  if (pr)
    if(MessageBoxW(0,L"程序 Motor.exe 已经运行, 需要重启程序吗？",L"Motor", MB_OKCANCEL|MB_ICONQUESTION|MB_SETFOREGROUND)!=IDOK)
      {
        return true;
      }
    else{
      while(pr){
        KillProcess(pr);
        pr = FindProcess(name);
      }
    }
  return false;

}
