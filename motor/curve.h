﻿///////////////////////////////////////////////////////////////////////////////
// Name:        chart
// Purpose:     
// Author:      bhd
// Modified by:
// Created:     2011-7-05
// Copyright:   (C) Copyright 2011, All Rights Reserved.
// Licence:     
///////////////////////////////////////////////////////////////////////////////

//stl

#pragma once

#include <math.h>
#include "curvedef.h"

class chart : public wxWindow
{
public:
	chart(wxWindow *parent,int id);
	~chart(void);
public:
	void addred(double x,double y);
	void addblue(double x,double y);

	void setData(const std::vector<std::shared_ptr<Point>>&data,bool isRed);
	void setData(const std::vector<std::shared_ptr<Point>>&dRed, const std::vector<std::shared_ptr<Point>>&dBlue);

	void clearAll();
	void setTitle(const wxString&t);
	void setXLabel(const wxString&t);
	void setYLabel(const wxString&t);

	inline void showTitle(bool show)
	{
		is_draw_title = show;
		this->refreshAll();
	};

	//自动绘制范围,默认关
	//void setAuto(bool a=false);

private:
	//最多支持2个通道读取和显示
	line red, blue;

	wxBitmap buf;

	unsigned int \
		titleH,
		widgSm;
	int smTextH;
	int cordLeft,
		cordRight,
		cordUp,
		cordBottom;
	int cordW,cordH;
	bool is_draw_title;

private:
	bool busy;

	wxString title,xlabel,ylabel;
	//因为是画在一张图里，所以只需一个框架
	double minx,miny,maxx,maxy;
	//屏幕坐标，只在需要更新最大最小值时重新计算
	std::vector<std::shared_ptr<wxPoint>> virRed,virBlue;
	
	//负责转换虚拟坐标到屏幕坐标
	void refreshAll();

	void clearVir();

	void paint(wxPaintEvent & evt);

	void realDraw(wxDC&  dc);
	void onSizeEvt(wxSizeEvent&e);

	DECLARE_EVENT_TABLE()

};

