#pragma once
#include "curvedef.h"
#include <vector>

class view
{
public:
	view(){};
	virtual ~view(){};

	virtual void status(const char* s, int id = 1) = 0;
	virtual void UpdateDelayLineChart() = 0;
	virtual void UpdateMotorPos(const wchar_t* m) = 0;
	virtual void SetMotorStepsize(const float& s)=0;
private:
};

extern view* app;