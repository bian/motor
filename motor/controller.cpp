﻿#include "stdh.h"
#include "viewInterface.h"
#include "deviceInterface.h"
#include "controller.h"

#include "motorhelper.h"

using namespace std;
view* app;
deviceInterface* dev;
controller* ctl;

boost::mutex mut;

controller::controller()
{
	pthread = 0;
	this->pflag= state_stop;
	this->read_delay = 0;

	cf = new cfg(CONF);
    cf->set_region_name(APP);

}
controller::~controller()
{
}

bool controller::setupdriver(){
  char str[255];
  if (!cf->get(MMKEY, str))  
    {    strcpy(str, MMV); cf->set(MMKEY, str);  }
  int id = atoi(str);
  if(!dev->loadMotorModule(id))
    return false;
  else{
	  // get paras
	  float mm;
	  dev->motor.get_um_per_step(&mm);
	  app->SetMotorStepsize(mm);
  }
  if (!cf->get(RMKEY, str))  {    strcpy(str, RMV); cf->set(RMKEY, str);  }
  id = atoi(str);
  if(!dev->loadReaderModule(id)){
    return false;}
  return true;
}
void controller::start()
{
	boost::thread work_thread(start_work);
}


void controller::getPortConfig(int* m, int*r)
{
  char str[255];
  if(!cf->get(motorport, str)){
    *m = 1;cf->set(motorport, "1");
  }
  else *m = atoi(str);
  if(!cf->get(readport, str)){
    *r = 2;cf->set(readport, "2");
  }
  else *r = atoi(str);
  portnum_motor = *m;
  portnum_read = *r;
}

void controller::setPortConfig(int m, int r)
{
    cf->set_int(motorport, m);
    this->portnum_motor = m;
    cf->set_int(readport, r);
    this->portnum_read = r;
}

bool controller::get_popup_hardware(){
  char str[5];
  if(!ctl->cf->get(POPUP_HARDWARE_SETTING_DLG,str)){
    ctl->cf->set(POPUP_HARDWARE_SETTING_DLG,"0");
    return true;
  }
  return (strcmp(str,"0")==0);
}

bool controller::openPort(int p, bool bMotor)
{
  if (bMotor){
    return dev->openMotorPort(p);
  }
  return dev->openReaderPort(p);
}

void controller::closePort(bool bMotor)
{
  if (bMotor)
    dev->closeMotorPort();
  else
    dev->closeReaderPort();
}

void start_work()
{
	lg<< "start delayline thread."<<endl;

	boost::mutex::scoped_lock lock(mut);
	app->status("步进电机运行中...");

  int steps = ctl->ndatanum;
  int axis = ctl->naxis + 1;
  bool forward= ctl->bor;
  int channel = ctl->nchannel + 1;
  int scanTimes = ctl->navt;

  auto bBack = []()->bool
  {
      return ctl -> nback == 1;
  };

  auto bSeakPeak= []()->bool
  {
      return ctl -> nback == 2;
  };

  lg<<"parameters: "<<endl\
  <<"channel[0,1,2]: "<<ctl->nchannel<<endl\
  <<"direction: "<<(ctl->bor?"foreward":"backward")<<endl\
  <<"scanTimes: "<<ctl->navt<<endl\
  <<"delay: "<<ctl->read_delay<<endl\
  <<"need back: "<<(bBack()?"yes":"no")<<endl\
  <<"need seeking peak point: "<<(bSeakPeak()?"yes":"no")<<endl;

  ctl->delaylinedata_red_y_aver.resize(scanTimes);
  ctl->delaylinedata_blue_y_aver.resize(scanTimes);

  int apath=0;//记录已走路程
  double bufx, bufy;
  char ms_[255];

  

  // Start scan

  // Save initial position
  float init_pos = 0;
  dev->motor.getPos(axis, &init_pos);
  //
  auto goto_initial_pos = [&]()->void
  {
      const char* back_msg = "Motor is getting back.";
      lg<<back_msg<<endl;
      app->status(back_msg); 
      dev->motor.goPos(axis, init_pos);
      if(!dev->motor.wait_stop(axis))
      {
          lg<<"wait error!"<<endl;
          throw "e";
      }
  };

  bool suceed=false;
  for(int si=0;si<scanTimes && ctl->pflag==state_run;si++)
  {
      // Get back if not first round
      if(si!=0)
      {
          goto_initial_pos();
      }

      ctl->delaylinedata_red_x.clear();
      ctl->delaylinedata_red_y.clear();
      ctl->delaylinedata_blue_x.clear();
      ctl->delaylinedata_blue_y.clear();
      ctl->delaylinedata_red_x.reserve(steps);
      ctl->delaylinedata_red_y.reserve(steps);
      ctl->delaylinedata_red_y_aver[si].reserve(steps);
    
    apath=0;
    for (int curPos = 0;curPos <steps  && ctl->pflag==state_run; curPos++)
    {
      dev->motor.move(axis, ctl->stepsize, forward);
      apath+=1;
      if (ctl->read_delay> 0)
        Sleep(ctl->read_delay);
      if (channel==1)
      {
        suceed=dev->readDivice.pgetx(&bufx);
        if(!suceed)
        {
            app->status("读取失败,已经停止"); 
            ctl->pflag=state_stop;
            return;
        }
        ctl->delaylinedata_red_x.push_back((double)curPos);
        ctl->delaylinedata_red_y.push_back(bufx);
        ctl->delaylinedata_red_y_aver[si].push_back(bufx);
      }
      else if (channel==2)
      {
        suceed=dev->readDivice.pgety(&bufy);
        if(!suceed)
        {
            app->status("读取失败,已经停止");
            ctl->pflag=state_stop;
            return;
        }
        ctl->delaylinedata_blue_x.push_back((double)curPos);
        ctl->delaylinedata_blue_y.push_back(bufy);
        ctl->delaylinedata_blue_y_aver[si].push_back(bufy);
      }
      else
      {
        suceed=dev->readDivice.pgetall(&bufx, &bufy);
        if(!suceed)
        {
            app->status("读取失败,已经停止"); 
            ctl->pflag=state_stop;
            return;
        }
        ctl->delaylinedata_red_x.push_back((double)curPos);
        ctl->delaylinedata_red_y.push_back(bufx);
        ctl->delaylinedata_blue_x.push_back((double)curPos);
        ctl->delaylinedata_blue_y.push_back(bufy);
        ctl->delaylinedata_red_y_aver[si].push_back(bufx);
        ctl->delaylinedata_blue_y_aver[si].push_back(bufy);
      }

      app->UpdateDelayLineChart();

      float percent=100.f*(si*steps+curPos+1.f)/(scanTimes*steps);
      sprintf(ms_, "已完成: %0.2f%%",percent);
      app->status(ms_, 2);

      // Pause
      while(ctl->pflag==state_pause) Sleep(800);
    }
    // Stop
    if(ctl->pflag==state_stop)
    {
      if(bBack())
      {
          goto_initial_pos();
      }
      app->status("电机已就绪.");
      return;
    }
  }
  // End of Scanning
  
  // Set flag
  ctl->pflag=state_stop;
  
  if(scanTimes!=1)
  {
    // Average  
    for(int index_=0;index_<steps;index_++)
    {
      if (channel==1 || channel==3)
      {
          for(int ki = 0; ki<scanTimes-1; ++ki)
              ctl->delaylinedata_red_y[index_] += ctl->delaylinedata_red_y_aver[ki][index_];
          ctl->delaylinedata_red_y[index_] /= scanTimes;
      }
      else if (channel==2 || channel==3)
      {
          for(int ki = 0; ki<scanTimes-1; ++ki)
              ctl->delaylinedata_blue_y[index_] += ctl->delaylinedata_blue_y_aver[ki][index_];
          ctl->delaylinedata_blue_y[index_] /= scanTimes;
      }
    }
    app->UpdateDelayLineChart();
  }

  if(bBack())
  {
      goto_initial_pos();
  }
  else if(bSeakPeak())
  {
    app->status("寻峰中...");
    int X_max;
    if(channel==2)
      X_max = h_seekPeak(ctl->delaylinedata_blue_y);
    else
      X_max = h_seekPeak(ctl->delaylinedata_red_y);

    lg<< "Seeked peak is at [step] "<<X_max<<"/"<<steps<<endl;

    dev->motor.move(axis, X_max*ctl->stepsize,!forward);
  }

  lg<< "Scanning over."<<endl;
  app->status("扫描结束.");

}
