#pragma once
#include "read_dev_head.h"
#include "motor_head.h"
#include "serialport.h"


class deviceInterface
{
public:
	deviceInterface(){};
	virtual ~deviceInterface(){};

	virtual bool loadMotorModule(const int&id) = 0;
	virtual bool loadReaderModule(const int&id) = 0;
	virtual bool openMotorPort(int) = 0;
	virtual bool openReaderPort(int) = 0;
	virtual void closeMotorPort() = 0;
	virtual void closeReaderPort() = 0;

	readerInterface readDivice;
	motorInterface motor;
	timeout_async_serial sp_m, sp_r;
};

extern deviceInterface* dev;