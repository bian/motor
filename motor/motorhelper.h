#pragma once
#include "curvedef.h"
#include <vector>
#include "assert.h"
using namespace std;


inline void h_fill_mX1(std::vector<double> &x, std::vector<double> &y, int m)
{
    x.reserve(m);
    y.reserve(m);
    for(int i=1;i<=m;i++)
    {
        x.push_back(i);
        y.push_back(0);
    }
};

inline void h_fill_mXn(std::vector<std::vector<double>>&x, std::vector<std::vector<double>>&y, int m, int n)
{
  x.reserve(m);
  y.reserve(m);
  for (int i = 0; i < m; ++i)
    {
      std::vector<double> xx;
      std::vector<double> yy;
      h_fill_mX1(xx, yy, n);
      x.push_back(xx);
      y.push_back(yy);
    }
};

inline int h_seekPeak(const std::vector<double> &x){
  assert(x.size()!=0);
  double Y_max = x[0]; int X_max = 0;
  for (size_t i=0; i!= x.size(); ++i)
  {
    if (Y_max <= x[i])
    {
      Y_max = x[i];
      X_max = i;
    }
  }
  return X_max;

};