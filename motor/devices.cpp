﻿#include "const.h"
#include "devices.h"
#include "filescan.h"
#include "assert.h"


void setup_motor_sdp_v2805c(motorInterface& dest, timeout_async_serial& com)
{
  dest.openCom=[&](int p)->bool{
    if(!com.open(p)) 
      return false; 
    return com.write("D7");
  };
  dest.closeCom=[&]()->void{com.close();};

  dest.move=[&](int axis, int steps,bool forward)->bool{
    for(int i=0;i<steps;++i){
      if(!com.write(forward?"4":"5")) 
        return false;
    }
    return true;
  };

  //dest.set_um_per_step=set_um_per_step;
  dest.get_um_per_step=[&](float* d)->bool{
    *d = 0.2f;
    return true;
  };
  dest.goPos=[&](int x, float p)->bool
  {
      float cur_p = 0;
      if(!dest.getPos(x, &cur_p)) return false;

      float len = p - cur_p;
      char dest_cmd[20];
      sprintf(dest_cmd, "L%7d", int(abs(len)*10));
      if (!com.write(dest_cmd)) return false; 
      const char* cmd = len>0 ? "1" : "0";// 1+, 0-
      if (!com.write(cmd)) return false; 
      
      return true;
  };
  dest.getPos=[&](int x, float* p)->bool// um
  {
    static char buf[50];
    if (!com.write("s")) return false; 
    memset(buf,0,50);
    com.read(buf, 11);
    // flag
    *p = atoi(buf+3)*0.1;
    return true;
  };

  dest.wait_stop = [&](int x)->bool
  {
      float pos1, pos2;
      if(dest.getPos(x, &pos1))
      {
          Sleep(200);
          if(dest.getPos(x, &pos2))
          {
              if(pos1 == pos2)
                  return true;
              else
                  return dest.wait_stop(x);
          }
      }
      return false;
  };
}

void setup_mc200(motorInterface& dest, timeout_async_serial& com)
{
  dest.openCom=[&](int p)->bool{return com.open(p, 19200);};
  dest.closeCom=[&]()->void{com.close();};

  dest.move=[&](int axis, int steps,bool forward)->bool{
    static char strr[50];
    sprintf(strr,"%c%c %d\r", forward?'+':'-', axis==1?'X':'Y', steps);
    return com.write(strr) && com.read_until(strr) && com.read_until(strr);
  };

  //dest.set_um_per_step=set_um_per_step;
  dest.get_um_per_step=[&](float* d)->bool{
    *d = 2.5f;
    return true;
  };
  dest.goPos=[&](int x, double p)->bool
  {
    static char s[50];
    sprintf(s, "M%c %ld\r", x==1?'X':'Y', long(p/2.5f));
    return com.write(s) && com.read_until(s) && com.read_until(s);
  };
  dest.getPos=[&](int x, float* p)->bool// um
  {
  
    static char s[50];
    sprintf(s, "?%c\r", (x == 1)?'X':'Y');
    if (!com.write(s)) {return false; }
    memset(s, 0, 50);
    if (!com.read_until(s)) {return false; }
    if(s[0]!='?' || s[1]!= ((x == 1)?'X':'Y') ) return false;
    *p = float(atof(s+3)*2.5f);
    return true;
  };
}

void setup_esp300(motorInterface& dest, timeout_async_serial& com)
{
  dest.openCom=[&](int p)->bool
  {
	  if( com.open(p, 19200))
	  {
		  com.write("1MO?\r");
		  com.write("2MO?\r");
		  com.write("1SN2\r");
		  com.write("2SN2\r");

		  return true;
	  }
	  return false;
  };
  dest.closeCom=[&]()->void{com.close();};

  dest.move=[&](int axis, int steps,bool forward)->bool{
	  char strr[50];
	  sprintf(strr,"%dPR%c%f\r", axis, forward?'+':'-', 1.25f*steps/1000.0);
	  return com.write(strr);
  };

  //dest.set_um_per_step=set_um_per_step;
  dest.get_um_per_step=[&](float* d)->bool{
    *d = 1.25f;
    return true;
  };
  dest.goPos=[&](int x, double p)->bool
  {
    return true;
  };
  dest.getPos=[&](int x, float* p)->bool// um
  {
    *p=1.0f;
    return true;
  };
}

void setup_stf_lock_in(readerInterface& dest, timeout_async_serial& com)
{
  dest.openCom=[&](int p)->bool{return com.open(p);};
  dest.closeCom=[&](){com.close();};

  dest.pgetx=[&](double*dat)-> bool
  {
    if(!com.write("OUTP?1\r")) return false;
    char data[100];
    bool res = com.read_until(data);
    if(!res) return false;
    *dat = atof(data);
    return true;
  };
  dest.pgety=[&](double*dat)->bool
  {
    if(!com.write("OUTP?2\r")) return false;
    char data[100];
    bool res = com.read_until(data);
    if(!res) return false;
    *dat = atof(data);
    return true;
  };
  dest.pgetall=[&dest](double* x, double* y)-> bool
  {
    return dest.pgetx(x) && dest.pgety(y);
  };
}

void setup_stf_gpib_lockin(readerInterface& dest)
{
    HMODULE pReader = NULL;
    if (pReader) FreeLibrary(pReader);
    pReader=::LoadLibraryA(".\\drv\\reader\\lockinstf_gpib.dll");
    if(!pReader)    {throw "load error";}
    else
    {
        typedef void (*setupReaderEngine)(readerInterface& );
        setupReaderEngine setupReader= (setupReaderEngine)::GetProcAddress(pReader, "setupReaderEngine");
        setupReader(dest);
    }

}

devices::devices(void)
{
  pMotor=0;
  pReader=0;
}
devices::~devices(void)
{
  if(pMotor)
    FreeLibrary(pMotor);
  if(pReader)
    FreeLibrary(pReader);
  this->closeMotorPort();
  this->closeReaderPort();
}
typedef void (*setupMotorEngine)(motorInterface& );
bool devices::loadMotorModule(const int&id){
	if(id==1)
        setup_motor_sdp_v2805c(motor,sp_m);
else if(id==0)
  setup_mc200(motor,sp_m);
else if(id==2)
  setup_esp300(motor,sp_m);
  return true;
}

bool devices::loadReaderModule(const int&id){
    if(id==0)
        setup_stf_lock_in(readDivice,sp_r);
    else if(id==1)
        setup_stf_gpib_lockin(readDivice);
    return true;
}


bool devices::openMotorPort(int p)
{
  return motor.openCom(p);
}
bool devices::openReaderPort(int p)
{
  return readDivice.openCom(p);
}

void devices::closeMotorPort()
{
  motor.closeCom();
}
void devices::closeReaderPort()
{
  readDivice.closeCom();
}

