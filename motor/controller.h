#pragma once

#include <vector>
#include "logger.hpp"
#include "cfg.h"
#define APP "motor"
#define CONF ".\\motor.ini" 

#define MMKEY "motor_module"
#define RMKEY "readdev_module"
#define MMV "0"
#define MMV2 "1"
#define MMV3 "2"
#define RMV "0"
#define RMV2 "1"

#define POPUP_HARDWARE_SETTING_DLG "popup_hardware_setting_dlg"


#define motorport "motor_port"
#define readport "readdev_port"

extern logger lg;
extern boost::mutex mut;

void start_work();

enum
  {
    state_run=1,
    state_pause,
    state_stop
  };

class controller
{
 public:
  controller();
  ~controller();

  bool setupdriver();
  inline void get_driver_cfg(int&m,int&r){
    char str[255];
    cf->set_region_name(APP);
    if (!cf->get(MMKEY, str))  {    m=0;strcpy(str, MMV); cf->set(MMKEY, str);  }
    else{
      m=atoi(str);
    }
    // if(!dev->loadMotorModule(str)){return false;}
    if (!cf->get(RMKEY, str))  
	{   
		r=0;
		strcpy(str, RMV); 
		cf->set(RMKEY, str);  
	}
    // if(!dev->loadReaderModule(str)){return false;}
    else{
      r=atoi(str);
    }
  };
  inline void set_drive_cfg(int&m,int&r){
    char ss[23];
    sprintf(ss,"%d",m);
    cf->set_region_name(APP);
    cf->set(MMKEY, ss);
    sprintf(ss,"%d",r);
    cf->set(RMKEY, ss);
  };
  
  bool get_popup_hardware();
  void set_popup_hardware(bool t){
      cf->set_region_name(APP);
      cf->set(POPUP_HARDWARE_SETTING_DLG,t?"0":"1");
  };

  void start();


  void getPortConfig(int* m, int*r);
  void setPortConfig(int m, int r);

  bool openPort(int p, bool bMotor);
  void closePort(bool bMotor);

 private:
  int portnum_motor, portnum_read;

 public:
     std::vector<double> delaylinedata_red_x, delaylinedata_red_y;
     std::vector<double> delaylinedata_blue_x, delaylinedata_blue_y;

     std::vector<std::vector<double>> delaylinedata_red_y_aver;
     std::vector<std::vector<double>> delaylinedata_blue_y_aver;


  HANDLE pthread;
  int pflag;
  cfg * cf;


  int ndatanum;
  int stepsize;
  bool bor;//orientation
  int ndiv;
  int naxis;
  int nchannel;//0, 1, 2
  int nback;//0, 1, 2
  int navt;//aver time
  int read_delay;//in ms
};

extern controller* ctl;


