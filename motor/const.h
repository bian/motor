﻿#ifndef WX_CONST_H
#define WX_CONST_H

#include "stdh.h"

#include "wx/wxprec.h"
#include "wx/wx.h"
#include "wx/xrc/xmlres.h"//xrc 
#include "wx/wxprec.h"
#include "wx\dialog.h"
#include <wx/tokenzr.h>
#include <wx/log.h>
#include <wx/event.h> 
#include "wx/file.h"
#include <wx/wfstream.h>
#include <wx/config.h>
#include <wx/fileconf.h>
#include <wx/confbase.h>
#include <wx/msw/regconf.h>
#include <wx/image.h>
#include "wx/intl.h"

#define msg wxMessageBox

#ifdef _DEBUG
	#define  wxMessageBox wxLogMessage
#endif

#endif

