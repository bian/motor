#pragma once
#include <vector>

class Point
{
public:
	Point(){}
	Point(double dx,double dy){x=dx;y=dy;}
	~Point(){}
	double x,y;
	inline void set(double x_, double y_){x=x_; y=y_;};
};


class line
{
public:
	line(  )
	{
		dline.reserve(20000);
	}
	~line(  )
	{
	}
	void add(double x,double y)
	{
		dline.push_back(std::shared_ptr<Point>(new Point(x,y)));
	}
	void clear()
	{
		/*for(unsigned int i=0;i!=dline.size();i++)
			delete dline[i];*/
		dline.clear();
	}
	std::vector<std::shared_ptr<Point>> dline;
	//wxList s;
	int getSize()
	{
		return dline.size();
	}
	double getx(int index)
	{
		return dline[index]->x;
	}
	double gety(int index)
	{
		return dline[index]->y;
	}
};