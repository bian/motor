﻿
#include "const.h"
#include "curve.h"
using namespace std;

//IMPLEMENT_DYNAMIC_CLASS(chart, wxWindow)
BEGIN_EVENT_TABLE(chart, wxWindow)

// catch paint events
EVT_PAINT(chart::paint)
EVT_SIZE(chart::onSizeEvt)
END_EVENT_TABLE()

chart::chart(wxWindow* parent,int id):title(wxT("")), xlabel(wxT("")), ylabel(wxT("")), wxWindow(parent,id)
{
  buf.Create(2000,2000);
  virRed.reserve(20000);
  virBlue.reserve(20000);
  this->clearVir();
  this->SetBackgroundStyle(wxBG_STYLE_CUSTOM);
  //初始化
  busy=false;

  titleH=30;
  widgSm=5;
  smTextH=20;
  is_draw_title = true;

  this->clearAll();
}


chart::~chart(void)
{
}
void chart::paint(wxPaintEvent & WXUNUSED(evt))
{
  wxPaintDC dc(this);
  //双缓冲
  wxSize sz=this->GetSize();
  static int ww = 0;
  static int hh = 0;
  if(ww != sz.GetWidth() || hh!= sz.GetHeight())
    {
      ww = sz.GetWidth();
      hh = sz.GetHeight();
      buf.Create(ww,hh);
    }

  wxMemoryDC mdc;
  mdc.SelectObject(buf);
  realDraw(mdc);
  mdc.SelectObject(wxNullBitmap);

  dc.DrawBitmap(buf.GetSubBitmap(wxRect(0,0,ww,hh)),0,0);
}

void chart::addred(double x,double y)
{
  if(red.getSize()==0&&blue.getSize()==0)
    {
      maxx=minx=x; 
      maxy=miny=y;
      red.add(x,y);
      refreshAll(); 
      return;
    }
  red.add(x,y);
  if(minx>x) { minx=x; }
  if(maxx<x) { maxx=x; }
  if(miny>y) { miny=y; }
  if( maxy<y) { maxy=y; }
  refreshAll();
}
void chart::addblue(double x,double y)
{
  if(blue.getSize()==0&&red.getSize()==0)
    {
      maxx=minx=x;  
      maxy=miny=y;
      blue.add(x,y);
      refreshAll(); 
      return;
    }
  blue.add(x,y);
  if(minx>x) { minx=x; }
  if(maxx<x) { maxx=x; }
  if(miny>y) { miny=y; }
  if( maxy<y) { maxy=y; }
	
  refreshAll();
}

void chart::clearAll()
{
  red.clear();blue.clear();
  minx=maxx=miny=maxy=0;
  clearVir();


}
void chart::setTitle(const wxString&t)
{          
  title=t;this->Refresh();
}
void chart::setXLabel(const wxString&t)
{                
  xlabel=t;this->Refresh();
}
void chart::setYLabel(const wxString&t)
{                
  ylabel=t;this->Refresh();
}
/*void chart::setAuto(bool a)
{
}*/
wxPen cordLine(wxColor(0,0,0),1);
wxPen cordWget(wxColor(0,0,0),1);
wxPen PointRed(*wxRED, 2);
wxBrush PointBrushRed(*wxRED, wxSOLID);

wxPen PointBlue(*wxBLUE, 2);
wxBrush PointBrushBlue(*wxBLUE, wxSOLID);
wxPen label(wxColor(0,0,255),1);
wxBrush br_back(wxColor(220,220,220));
//virtual line
wxPen pen_vl(wxColor(200,200,200),1,wxDOT);




const wxFont normalFont( 8, wxSWISS, wxNORMAL, wxNORMAL );


void chart::realDraw(wxDC&  dc)
{
  int ww,hh;
  this->GetSize(&ww,&hh);
  //
  dc.SetBrush(br_back);
  dc.DrawRectangle(0,0,ww,hh);
  dc.SetBrush(wxBrush(*wxWHITE, wxSOLID));
  dc.DrawRectangle(cordLeft,cordUp,cordW,cordH);
  //title, x/y label
  //dc.SetBrush(*wxBLUE_BRUSH); // blue filling
  //   dc.SetPen( wxPen( wxColor(255,175,175), 10 ) ); // 10-pixels-thick pink outline
  //   dc.DrawRectangle( 300, 100, 400, 200 );
  
  if(is_draw_title)
  {
	  dc.SetPen( label );
	  dc.SetFont( wxFont( 18, wxSWISS, wxNORMAL, wxNORMAL ) );
	  dc.DrawLabel(title,wxRect(0,0,ww,titleH),wxALIGN_CENTER);
  }

  dc.SetFont( normalFont );
  dc.DrawRotatedText(ylabel ,2 ,\
                     hh/2+dc.GetTextExtent(ylabel).GetWidth()/2 , 90);
  dc.DrawText(xlabel, cordLeft+(cordW)/2-dc.GetTextExtent(xlabel).GetWidth()/2, hh-smTextH+2);

  //axis
  dc.SetPen( cordLine );
  //dc.DrawLine( cordLeft, cordUp,               cordLeft, cordBottom );

  //画标记和坐标值前先判断点的个数
  int sum_size=red.getSize()+blue.getSize();
  dc.SetPen(pen_vl);
  if(sum_size==0 )
    {
      //x	--->
      dc.SetPen(cordWget);
      dc.DrawLine( cordLeft+(cordW)/2,  cordBottom, cordLeft+(cordW)/2, cordBottom+ widgSm);
      dc.SetPen(pen_vl);
      dc.DrawLine(cordLeft+(cordW)/2, cordBottom, cordLeft+(cordW)/2, cordUp);
      static const wxString NotAvail=L"N/A";
      wxSize sz=dc.GetTextExtent(NotAvail);
      dc.DrawText(NotAvail,  cordLeft+(cordW)/2-sz.GetWidth()/2, cordBottom+widgSm+2);
      //y	↓
      dc.SetPen(cordWget);
      dc.DrawLine(cordLeft-widgSm, cordUp+(cordH)/2, cordLeft, cordUp+(cordH)/2);
      dc.SetPen(pen_vl);
      dc.DrawLine(cordLeft, cordUp+(cordH)/2, cordRight, cordUp+(cordH)/2);
      dc.DrawText(NotAvail, smTextH+30, cordUp+(cordH)/2-smTextH/2);
    }
  else if(sum_size==1)
    {
      //x
      dc.SetPen(cordWget);
      dc.DrawLine( cordLeft+(cordW)/2,  cordBottom, cordLeft+(cordW)/2, cordBottom+ widgSm);
      dc.SetPen(pen_vl);
      dc.DrawLine(cordLeft+(cordW)/2, cordBottom, cordLeft+(cordW)/2, cordUp);
      wxString lab;
      lab.Printf(_T("%   .2e"),miny);
      wxSize sz=dc.GetTextExtent(lab);
      dc.DrawText(_T("1"), cordLeft+(cordW)/2-2, cordBottom+widgSm+2);
      //y
      dc.SetPen(cordWget);
      dc.DrawLine(cordLeft-widgSm, cordUp+(cordH)/2, cordLeft, cordUp+(cordH)/2);
      dc.SetPen(pen_vl);
      dc.DrawLine(cordLeft, cordUp+(cordH)/2, cordRight, cordUp+(cordH)/2);
      dc.DrawText(lab, smTextH, cordUp+(cordH)/2-smTextH/2);
    }
  else
    {
      wxString lab;
      wxSize sz;
      //x标记
      int start=cordLeft;
      int lastindex = 0;
      while(start<=cordRight)
        {
          int lb = (int)(minx+(maxx-minx)*(start-cordLeft)/(cordRight-cordLeft));
          //avoid same step label
          if(lastindex == lb){
            start+=24*5;
            continue;
          }
          lastindex = lb;
          dc.SetPen(cordWget);
          dc.DrawLine( start,  cordBottom, start, cordBottom+ widgSm);
          dc.SetPen(pen_vl);
          if(start!=cordLeft)
            dc.DrawLine(start, cordBottom,start, cordUp);
          lab.Printf(_T("%d"),lb);
          sz=dc.GetTextExtent(lab);
          dc.DrawText(lab,start-sz.GetWidth()/2, cordBottom+widgSm+2);
          start+=24*5;
        }
      //y轴标记
      start=cordBottom;
      while(start>=cordUp)
        {
          dc.SetPen(cordWget);
          dc.DrawLine( cordLeft,  start, cordLeft-widgSm, start );
          dc.SetPen(pen_vl);
          if(start!=cordBottom)
            dc.DrawLine(cordLeft, start,cordRight, start);
          lab.Printf(_T("%   .2e"),miny+(maxy-miny)*(cordBottom-start)/(cordBottom-cordUp));
          sz=dc.GetTextExtent(lab);
          dc.DrawText(lab,smTextH,  start-smTextH/2);
          start-=24*5;
        }

    }

  //再画曲线
  //直接将屏幕坐标画出
  //red和blue数目并不一样多
  dc.SetPen( PointRed );
  dc.SetBrush(PointBrushRed);
  unsigned int size=virRed.size();
  for(unsigned int ii=0;ii<size;ii++)
    {
      dc.DrawCircle( virRed[ii]->x,  virRed[ii]->y,3);
    }
  dc.SetPen( PointBlue );
  dc.SetBrush(PointBrushBlue);
  size=virBlue.size();
  for(unsigned int ii=0;ii<size;ii++)
    {
      dc.DrawCircle( virBlue[ii]->x,  virBlue[ii]->y,3);
    }

}
void chart::onSizeEvt(wxSizeEvent&WXUNUSED(e))
{
  refreshAll();
}
void chart::clearVir()
{
  virRed.clear();
  virBlue.clear();
}
void chart::refreshAll()
{
  //将每个坐标转化为屏幕坐标
  //公式为 屏幕坐标x-50=(dx-minx)/(maxx-minx)*ww
  int ww,hh;
  this->GetSize(&ww,&hh);

  wxMemoryDC mdc;
  mdc.SetFont( normalFont );
  smTextH=mdc.GetTextExtent(L"text").GetHeight()+4;
  
  static int ylable_wid = mdc.GetTextExtent(L"100.27e+899").GetWidth();
  cordLeft=smTextH+ylable_wid+widgSm;
  cordRight=ww-smTextH;
  if(is_draw_title)
	  cordUp=titleH;
  else
	  cordUp = 10;
  cordBottom=hh-2*smTextH-widgSm;

  cordW=cordRight-cordLeft;
  cordH=cordBottom-cordUp;

  //
  clearVir();

  size_t size=red.getSize();
  if(size == 1){
    auto pp=shared_ptr<wxPoint>(new wxPoint());
      
    pp->x=(int)(cordLeft + .5*cordW);
    pp->y=(int)(cordUp + .5*cordH);
    virRed.push_back(pp);
  }
  else
    for(size_t ii=0;ii<size;ii++)
      {
        auto pp=shared_ptr<wxPoint>(new wxPoint());
        pp->x=(int)(cordLeft+(red.getx(ii)-minx)/(maxx-minx)*cordW);
        pp->y=(int)(cordUp+(maxy-red.gety(ii))/(maxy-miny)*cordH);
        virRed.push_back(pp);
      }

  size=blue.getSize();
  if(size == 1){
    auto pp=shared_ptr<wxPoint>(new wxPoint());
      
    pp->x=(int)(cordLeft + .5*cordW);
    pp->y=(int)(cordUp + .5*cordH);
    virBlue.push_back(pp);
  }
  else
    for(size_t ii=0;ii<size;ii++)
      {
        auto pp=shared_ptr<wxPoint>(new wxPoint());
        pp->x=(int)(cordLeft+(blue.getx(ii)-minx)/(maxx-minx)*cordW);
        pp->y=(int)(cordUp+(maxy-blue.gety(ii))/(maxy-miny)*cordH);
        virBlue.push_back(pp);
      }

  this->Refresh();
}

void chart::setData(const std::vector<shared_ptr<Point>>&data,bool isRed)
{
  if(isRed)
    {
      red.dline=data;
    }
  else
    {
      blue.dline=data;
    }
  refreshAll();
}

void chart::setData(const std::vector<shared_ptr<Point>>&dRed, const std::vector<shared_ptr<Point>>&dBlue)
{
  busy=true;
  this->Refresh();
  red.dline=dRed;
	
  blue.dline=dBlue;
  //更新边界
  size_t redSize=red.dline.size(),
    blueSize=blue.getSize();
  bool Inited=false;
  for(size_t i=0;i<redSize;i++)
    {
      if(i==0)
        {
          minx=red.dline[0]->x;
          maxx=red.dline[redSize-1]->x;
          maxy=miny=red.dline[0]->y;
          Inited=true;
        }

      if(red.dline[i]->y>maxy)
        maxy=red.dline[i]->y;
      if(red.dline[i]->y<miny)
        miny=red.dline[i]->y;
    }

  for(size_t i=0;i<blueSize;i++)
    {
      if(i==0 && !Inited)
        {
          minx=blue.dline[0]->x;
          maxx=blue.dline[blueSize-1]->x;
          maxy=miny=blue.dline[0]->y;
        }
      if(blue.dline[i]->y>maxy)
        maxy=blue.dline[i]->y;
      if(blue.dline[i]->y<miny)
        miny=blue.dline[i]->y;
    }

  refreshAll(); 
  busy=false;
}


