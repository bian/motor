﻿#pragma once
#include "windows.h"
#include "deviceInterface.h"

class devices: public deviceInterface {
public:
	devices(void);
	~devices(void);

	bool loadMotorModule(const int&id);
	bool loadReaderModule(const int&id);
	bool openMotorPort(int);
	bool openReaderPort(int);
	
	void closeMotorPort();
	void closeReaderPort();

private:
	HMODULE pMotor,pReader;

};



