/*
*   斯坦福锁相 gpib 接口
*
*/
#include "read_dev_head.h"
// #include "helper.h"
#include "ni4882.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "windows.h"

int Device = 0;                   /* Device unit descriptor                  */
int BoardIndex = 0;               /* Interface Index (GPIB0=0,GPIB1=1,etc.)  */
char Buffer[101];

BOOL APIENTRY DllMain( HANDLE hModule,
    DWORD  ul_reason_for_call, 
    LPVOID lpReserved
    )
{
    switch( ul_reason_for_call )
    {
    case DLL_PROCESS_ATTACH:break;
    case DLL_THREAD_ATTACH:break;
    case DLL_THREAD_DETACH:break;
    case DLL_PROCESS_DETACH:break;
    }
    return TRUE;
}


bool query(const char* cmd, double* out)
{
    ibwrt(Device, cmd , strlen(cmd));     /* Send the identification query command   */
   if (Ibsta() & ERR) {
      return false;
   }

   ibrd(Device, Buffer, 100);     /* Read up to 100 bytes from the device    */
   if (Ibsta() & ERR) {
      return false;  
   }

   Buffer[Ibcnt()] = '\0';        /* Null terminate the ASCII string         */
   *out = atof(Buffer); 

    return true;
}
//
bool getx(double*dat) 
{
    return query("OUTP?1\r", dat);
}
bool gety(double*dat) 
{
    return query("OUTP?2\r", dat);
}
bool getall(double* x, double* y) 
{
    return getx(x) && gety(y);
}

bool openCom(int p)
{
    int   PrimaryAddress = p;      /* Primary address of the device           */
   int   SecondaryAddress = 0;    /* Secondary address of the device         */
   char  Buffer[101];             /* Read buffer                             */

   Device = ibdev(                /* Create a unit descriptor handle         */
         BoardIndex,              /* Board Index (GPIB0 = 0, GPIB1 = 1, ...) */
         PrimaryAddress,          /* Device primary address                  */
         SecondaryAddress,        /* Device secondary address                */
         T10s,                    /* Timeout setting (T10s = 10 seconds)     */
         1,                       /* Assert EOI line at end of write         */
         0);                      /* EOS termination mode                    */
   if (Ibsta() & ERR) {           /* Check for GPIB Error                    */
    return false;
}

   ibclr(Device);                 /* Clear the device                        */
if (Ibsta() & ERR) {
    return false;
}
return true;
}

void closeCom()
{
    ibonl(Device, 0);
}
extern "C" __declspec(dllexport) void setupReaderEngine(readerInterface& dest)
{
    dest.openCom=openCom;
    dest.closeCom=closeCom;

    dest.pgetx=getx;
    dest.pgety=gety;
    dest.pgetall=getall;
}


