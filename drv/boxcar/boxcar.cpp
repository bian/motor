﻿
#include "serialport.h"
#include "read_dev_head.h"

sync_serialport com;

BOOL APIENTRY DllMain( HANDLE hModule,
	DWORD  ul_reason_for_call, 
	LPVOID lpReserved
	)
{
	switch( ul_reason_for_call )
	{
	case DLL_PROCESS_ATTACH:break;
	case DLL_THREAD_ATTACH:break;
	case DLL_THREAD_DETACH:break;
	case DLL_PROCESS_DETACH:
		com.close();
	}

	return TRUE;
}

auto send_and_get = [](const char* command, char * out)->bool{
	if(!com.write(command)) return false;
	std::string s;
	com.readuntil(s, '\r');
	strcpy(out, s.c_str());
	return true;
};
bool getx(char*dat, int len = 255)
{
	return send_and_get("?1\r", dat);
}
bool gety(char*dat, int len = 255) 
{
	return send_and_get("?2\r", dat);
}
bool getall(char* x, char* y, int len = 255) 
{
	if(!send_and_get("?1\r", x)) return false;
	return send_and_get("?2\r", y);
}


bool openCom(int portnum)
{
	return com.open(portnum);
}

void closeCom()
{
	com.close();
}
extern "C" __declspec(dllexport) void setupReaderEngine(readerInterface& dest)
{
	dest.openCom=openCom;
	dest.closeCom=closeCom;

	dest.pgetx=getx;
	dest.pgety=gety;
	dest.pgetall=getall;
}


