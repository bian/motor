﻿#pragma once
#include <functional>
using namespace std;


struct readerInterface
{
	function<bool(int)> openCom;
	function<void()> closeCom;

	function<bool(double*)> pgetx;
	function<bool(double*)> pgety;
	/**
	 * len is a length > x+y
	 */
	function<bool(double*, double*)> pgetall;
};
