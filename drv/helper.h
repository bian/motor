#pragma once
#include "stdlib.h"
#include "stdio.h"
#include <string.h>

inline int h_find(const char* cr, char c, size_t t)
{
  size_t k = 0;
  for (size_t i = 0; i < strlen(cr); ++i)
    {
      if (cr[i] == c)
        {
          if( ++k == t)
            return int(i);
        }
    }
  return -1;

};

inline int h_getdivide(const char* c)
{
  int i3 = h_find(c, ',', 3);
  int i4 = h_find(c, ',', 4);
  if(i3 == -1 || i4 == -1 || (i3==i4-1)) return -1;

  char dd[5];
  memset(dd, 0, 5);
  strncpy(dd, c + i3 + 1, i4-i3-1);
  return atoi(dd);
};

inline bool getxy(const char*src, char*x, char*y){
  int id = h_find(src, '\r', 1);
  strncpy(x, src, id);
  strcpy(y, src + id + 1);
  //std::cout<<x<<" "<<y<<std::endl;
  return id != -1;
};