﻿
#include "serialport.h"
#include "read_dev_head.h"

serialPort com;
//
BOOL APIENTRY DllMain( HANDLE hModule,
	DWORD  ul_reason_for_call, 
	LPVOID lpReserved
	)
{
	switch( ul_reason_for_call )
	{
	case DLL_PROCESS_ATTACH:break;
	case DLL_THREAD_ATTACH:break;
	case DLL_THREAD_DETACH:break;
	case DLL_PROCESS_DETACH:
		com.close();
	}
	return TRUE;
}


//
bool getx(char*dat, int len = 255) 
{
	com.clearbuf(true);
	com.write("X.\r");
	Sleep(200);
	return com.read(dat, 200);
}
bool gety(char*dat, int len = 255) 
{
	com.clearbuf(true);
	if(!com.write("Y.\r"))
	{
		return false;
	}
	Sleep(200);

	return com.read(dat,200);
}
bool getall(char* x, char* y, int len = 255) 
{
	// TODO
	/*
	com.clearbuf(true);
	if(!com.write("X.;Y.\r"))
	{
		return false;
	}
	Sleep(200);

	return com.read(dat,200);
	*/
}


bool openCom(int portnum)
{
	if( com.open(portnum))
	{
		return true;
	}
	return false;
}

void closeCom()
{
	com.close();
}

extern "C" __declspec(dllexport) void setupReaderEngine(readerInterface& dest)
{
	dest.openCom=openCom;
	dest.closeCom=closeCom;

	dest.pgetx=getx;
	dest.pgety=gety;
	dest.pgetall=getall;
}


