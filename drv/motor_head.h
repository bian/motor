﻿#pragma once
#include <functional>
using namespace std;

struct motorInterface
{
	function<bool(int)> openCom;
	function<void()> closeCom;
	// 1 base
	function<bool(int, int, bool)> move;

	function<bool(int, float)> goPos;
	function<bool(int,float*)> getPos;

	//bool (*set_um_per_step)(float d);
	function<bool(float*)> get_um_per_step;
	function<bool(int)> wait_stop;
};

//extern serialPort com;
